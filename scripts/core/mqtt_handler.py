import time
import csv
class Mqtt_Publish:
    @staticmethod
    def mqtt_publish(client):
        def on_message(client, userdata, message):
            print("message received ", str(message.payload.decode("utf-8")))
            print("message topic=", message.topic)
            print("message qos=", message.qos)
            print("message retain flag=", message.retain)
        client.on_message = on_message
        client.subscribe("Kwh")
        client.subscribe("kVAh")
        client.subscribe("kW")
        client.subscribe("kVA")
        client.subscribe("current")

        with open('C:\sample.csv', mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for row in csv_reader:
                client.loop_start()
                client.publish("Kwh", str({"Timestamp": row["Timestamp"], "Value": row['kWh']}))
                client.publish("kVAh", str({"Timestamp": row["Timestamp"], "Value": row['kVAh']}))
                client.publish("kW", str({"Timestamp": row["Timestamp"], "Value": row['kW']}))
                client.publish("kVA", str({"Timestamp": row["Timestamp"], "Value": row['kVA']}))
                client.publish("current", str({"Timestamp": row["Timestamp"], "Value": row['current']}))
                client.loop_stop()
